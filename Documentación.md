## DOCUMENTACIÓN APLICACIÓN SOA

***Nombre del Tema:* APLICACIÓN SOA**

***Autor:* Ana Lucrecia Villatoro Rodriguez - 201222595**

***Creado:* 12/08/2019**

***Versión:* 1.0** 

##ÍNDICE
1. INTRODUCCIÓN
2. OBJETIVOS
3. CARACTERISTICAS TECNICAS
	1. HARDWARE
	2. SOFTWARE
4. INTALACIÓN
5. DEFINICION DE LOS MICROSERVICIOS DE LA APLICACIÓN
6. INTERACCIÓN DE LOS MICROSERVICIOS Y ORQUESTADOR
	1. DESCRIPCIÓN
	2. DIAGRAMA
7. GLOSARIO

##INTRODUCCIÓN
Con la finalidad de aprender a implementar microservicios se desarrolló una aplicacion SOA para simular tres servicios para carros tipo Uber, estos son los siguientes:

1. Solicitud de servicio por parte del cliente
2. Recepción de solicitud y aviso al piloto
3. Solicitud de ubicación (rastreo) desde la administración del servicio de carros.

Y los servicios del ESB se desarrollan desde cero tambien para orquestar los microservicios.

##OBJETIVOS
###GENERAL
Desarrollar una aplicación soa que debe implementar microservicios y un orquestador.
###ESPECIFICOS
1. Desarrollar un microservicio que reciba la petición de un servicio de uber por el usuario.
2. Desarrollar un microservicio que de aviso al piloto que hay una solicitud de uber.
3. Desarrollar un microservicio que solicite ubicación.
##CARACTERISTICAS TECNICAS
###HARDWARE
Requisitos:

1. Procesador a 1,6 GHz o superior
2. 1 GB de RAM (1,5 GB si se ejecuta en una máquina virtual)
3. 20 GB de espacio disponible en el disco duro

###SOFTWARE
Esta aplicación fue desarrollada en

* Node js + express, Version de Node js: 1.16.1.
* Se utilizo Gitlab para control de versiones.
* MarkdownPad2, para documentacion.
* Postman como apoyo para prueba de servicio.

Debe de contar con un navegador web instalado.
	
##INTALACIÓN
###GitLab
1. Creacion de Repositorio
	1. Inicio de sesion en Gitlab, y dirigirse a la sección para crear un nuevo proyecto. ![Alt text](creacionrepositorio1.png)
	2. Colocar nombre del proyecto. ![Alt text](creacionrepositorio2.png)

2. Creacion de SSH para acceso local

	1. Generar

![Alt text](creacionssh1.png)

	2. Enlazar

![Alt text](creacionssh2.png)

3. Configuraciones globales

	1. Nombre Usuario

![Alt text](configuser1.png)

	2. Correo Usuario

![Alt text](configuser2.png)

4. Clonar Repositorio

	1. Clonar

 ![Alt text](clonarrepo.png)

	2. Acceder 

![Alt text](accederalrepo.png)

###APLICACIÓN
1. Descargar NodeJs de la [pagina oficial](https://nodejs.org/es/download/) e instalar siguiendo el wizar
![Alt text](descarganode.png)
2. Verificar la version de node
![Alt text](versionnode.png)
3. Crear proyecto dentro del repositorio que creamos
![Alt text](crearproyecto.png)
4. Correr el comando $ npm install, dentro del proyecto que acabamos de crear desde nuestro editor preferido
![Alt text](npminstall.png)
##DEFINICION DE LOS MICROSERVICIOS DE LA APLICACIÓN
Los microservicios que se realizaron en esta aplicación SOA son los siguientes:

1. Solicitud de servicio por parte del cliente
2. Recepción de solicitud y aviso al piloto
3. Solicitud de ubicación (rastreo) desde la administración del servicio de carros.
##INTERACCIÓN DE LOS MICROSERVICIOS Y ORQUESTADOR
###DESCRIPCIÓN
1. EL cliente realiza una solicitud de servicio.
2. El orquestador recibe la peticion y llama al microservicio recepción de solicitud.
3. Seguidamente el orquestador llama al microservicio que da aviso al piloto que hay un vieja.
4. El piloto requiere la ubicacion.
5. El orquestador recibe la peticion y llama al servicio que devulve la ubicacion del cleinte.
###DIAGRAMA
![Alt text](InteraccionMicroserviciosESBTarea2y3LabSA.png)
##GLOSARIO
###ESB
El Bus de Servicio Empresarial, representa el elemento de software que media entre las aplicaciones empresariales y permite la comunicación entre ellas. funciona como conector de aplicaciones en una arquitectura orientada a servicios (SOA). Implementan interfaces estandarizadas para proveer comunicación, conectividad, transformación, portabilidad y seguridad, a partir de la sincronización de los servicios y la asincrónica de los eventos.  
###MICROSERVICIO
Los microservicios son unidades funcionales concretas e independientes, que trabajan juntas para ofrecer la funcionalidad general de una  aplicación. Cada microservicio puede ser actualizado o escalado sin que esto afecte a la disponibilidad de los demás unidades y de la aplicación en su conjunto. Con los microservicios, las aplicaciones se dividen en sus componentes más pequeños e independientes entre sí. 
###UBICACIÓN
Una ubicación es un lugar, un sitio o una localización donde está ubicado algo o alguien. En este contexto
###APLICACIÓN
 Programa informático creado para llevar a cabo o facilitar una tarea en un dispositivo informático.
Las aplicaciones nacen de alguna necesidad concreta de los usuarios, y se usan para facilitar o permitir la ejecución de ciertas tareas en las que un analista o un programador ha detectado una cierta necesidad. 
###DESARROLLAR
Podemos definir el desarrollo de sistemas informáticos como el proceso mediante el cual el conocimiento humano y el uso de las ideas son llevados a las computadoras; de manera que pueda realizar las tareas para la cual fue desarrollada.
###ORQUESTACIÓN
La orquestación de servicios web se basa en un modelo centralizado en el cual las interacciones no se realizan directamente entre los servicios web sino que existe una entidad encargada de definir la lógica de interacción.
