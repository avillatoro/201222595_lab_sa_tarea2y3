var express = require('express');
var router = express.Router();
var ubicacion = require('./solicitudubicacion')

router.route('/recibirsolicitud')    
    .get(function (req, res) {
        res.render('avisoalpiloto', { title: 'Usuario ha solicitado servicio uber...' });
    });

router.route('/solicitudubicacion')    
    .get(function (req, res) {
        /* Aqui pretendo llamar al servicio de solicitudUbicacion pero no puede
            lo que esta actualmente no funciona */
        var laubicacion = ubicacion.get('/solicituddevolucionubicacion');
        console.log('laubicacion : ' + laubicacion);
        res.status(200).json({
            laubicacion: laubicacion
        });
    });    

module.exports = router;