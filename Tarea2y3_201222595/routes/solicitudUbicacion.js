var express = require('express');
var router = express.Router();
var geoip = require('geoip-lite');

/*
Se creo un array de ip para dar una ubicacion random
*/
var arrayIP = ["190.148.52.91", "190.148.62.91", "201.222.70.3", "201.222.60.40", "201.222.490.0", "201.222.30.255"];

router.route('/solicituddevolucionubicacion')
    .get(function (req, res) {
        var rand = Math.floor((Math.random() * 5) + 1);
        console.log('rand ' + rand);
        var ip = arrayIP[rand]
        console.log('ip ' + ip);
        var geo = geoip.lookup(ip);
        console.log('geo ' + geo);
        res.status(200).json({
            geo: geo
        });
    });


module.exports = router;
