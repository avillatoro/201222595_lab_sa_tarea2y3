var express = require('express');
var router = express.Router();

router.get('/', function(req, res) {
    res.render('solicitudCliente', { title: 'Solicitud de servicio' });
  });

module.exports = router;
